package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/*Classe Utils - Cria��o de uma classe Global, acess�vel a todas os projetos do workspace
 * Passo 1 - Criar o projeto JavaFX_00_Utils e nesse projeto criar a classe utils
 * 			Copiar o m�todo display da classe AlertBox do projeto 5a, para a nova classe Utils
 * 			Na classe Utils, Alterar o nome do m�todo display para alertbox()
 * 			Eliminar a classe main do projeto JavaFX_00_Utils
 * 			A classe utils tem agora o seu primeiro m�todo geral:
 * Passo 2 - criar acesso � classe geral Utils, neste e em futuros projetos do workspace:
 * 			a - Informar a este projeto a localiza��o da classe utils
 * 				Rato dr� sobre o nome do projeto - Proprieties
 * 				Java ClassPath -> Projects . Add (Mostra os projetos do workspace : Escolher o JavaFX_00_Utils) - ok
 * 			b - Neste projeto, na zona dos imports escrever : 
 * 				import application.Utils;
 * 			c - Os m�todos j� estar�o dispon�veis , bastando escrever Utils.
 * 
 * Nota : se o projeto global tiver um package diferente o processo � o mesmo : import package.Class ( o caminho � dado pelo passo 1)
 * */
public class Main extends Application {
	Button btnAlert;
//Cria Bot�o null, fora dos m�todos para ser acess�vel a todos
	@Override
	public void start(Stage primaryStage) {
		try {
		
			btnAlert = new Button("Clica-me");						//Inicializa��o
			btnAlert.setOnAction(e->Utils.alertbox("Cuidado", "isto � uma AlertBox"));
			
			StackPane layoutRoot = new StackPane(); //layout Principal
			layoutRoot.getChildren().add(btnAlert);
			
			Scene scene = new Scene(layoutRoot, 400,600);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Janela Modal");
			primaryStage.show();
			
			
			
			
			
			
			
		}
		catch(Exception e) {					//Tratamento Gen�rico das Exe��es
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}
